﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Text;


namespace Ex1API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            //Main call for the Api.


            DataService ds = new DataService();
            ds.GetData();

            FileStream stream = new FileStream("./log.json", FileMode.Append);

            using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
            {
                writer.WriteLine("{\"Date\":\"" + DateTime.Now + "\", \"Result\": [" + JsonConvert.SerializeObject(Format()) + "]},");
            }

            return Ok(Format());
        }
        
        //Method to fill the parameter of the city class.
        public City Format() 
        {
            var _city = new City();
            ApiFilter temp = new ApiFilter();

            _city.name = DataService.res_city[DataService.cityID].city;
            _city.country = DataService.res_country[DataService.countryID].name;
            _city.language = DataService.res_country[DataService.countryID].languages[0].name;

            foreach (var p in DataService.res_country)
            {
                if (DataService.chosenCountry.languages[0].iso639_1 == p.languages[0].iso639_1)
                {
                    if (p.population > temp.population)
                    {
                        temp = p;
                    }
                }
            }
            _city.mostLanguage = temp.name;

            return _city;
        }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        // GET api/logs
        [HttpGet]        
        public ActionResult<IEnumerable<string>> Get()
        {
            //Get for the Log
            string json;

            FileStream stream = new FileStream("./log.json", FileMode.Append);

            using (StreamReader reader = new StreamReader("log.json"))
            {
                json = reader.ReadToEnd();
            }
            return Ok(JsonConvert.DeserializeObject("[" + json + "]"));
        }
    }
    //Class to build Json Format.
    public class City
    {
        public string name;
        public string country;
        public string language;
        public string mostLanguage;
    }
}


