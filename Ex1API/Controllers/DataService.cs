using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;


namespace Ex1API
{
    public class DataService
    {
        // Global Variables
        public static string response;
        public static string regions;
        public static string citys;

        public static string alphacode;

        public static string region_name;
        public static Random rnd = new Random();

        public static IList<ApiFilter> res_country;
        public static IList<RegionFilter> res_region;
        public static IList<CityFilter> res_city;

        public static int countryID;
        public static int regionID;
        public static int cityID;

        public static ApiFilter chosenCountry;
        public static string ApiKey = "a5f22d1e50cdc891e195a9860262a662";


        public async void GetData()
        {
            // Request to Get a full list of country from Api.
            string baseUrl = "https://restcountries.eu/rest/v2/all?fields=name;population;languages;alpha2Code";
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(baseUrl))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    response = data;
                }
            }

            // Converting the eesponse stream into a Json Object.
            res_country = JsonConvert.DeserializeObject<IList<ApiFilter>>(response); 

            countryID = rnd.Next(0, res_country.Count() - 1); // Random ID

            chosenCountry = res_country[countryID]; //

            alphacode = chosenCountry.alpha2Code;// Paramter for the URL search (country).
            GetRegion();
        }

        public async void GetRegion()
        {
            // Request to Get the Region of the Chose Country.
            string regionUrl = "http://battuta.medunes.net/api/region/" + alphacode + "/all/?key=" + ApiKey;
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(regionUrl))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    regions = data;
                }
            }

            res_region = JsonConvert.DeserializeObject<IList<RegionFilter>>(regions);


            if (res_region.Count() == 0)
            {
                GetData();
            }
            else
            {
                regionID = rnd.Next(0, res_region.Count() - 1);
                DataService.region_name = res_region[regionID].region;
                GetCity();
            }
        }

        public async void GetCity()
        {   
            //Request to Get a Random City within tge Region.
            string cityUrl = "http://battuta.medunes.net/api/city/" + alphacode + "/search/?region=" + region_name + "&key=" + ApiKey;
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(cityUrl))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != "[]")
                {
                    citys = data;
                }
            }

            res_city = JsonConvert.DeserializeObject<IList<CityFilter>>(citys);
            

            if (res_city.Count() == 0)
            {
                GetData();
            }
            else
            {
                cityID = rnd.Next(0, res_city.Count() - 1);
            }
        }
    }

    // Class Section 
    public class ApiFilter
    {
        public language[] languages;

        public String name;

        public int population;

        public String alpha2Code;
    }

    public class language
    {
        public String iso639_1;
        public String iso639_2;
        public String name;
        public String nativeName;
    }

    public class RegionFilter
    {
        public String region;
        public String country;
    }
    public class CityFilter
    {
        public String city;
        public String region;
        public String country;
    }

}
